/* Express */
const express = require("express");


const app = express();

// Configuración de acceso a variables de entorno
require("./config/dotenv.config");

// Configuración de Passport
require("./config/passport.config")(app);

// Configuración general
app.set("port", process.env.PORT || 3000);

// Middlewares
if (process.env.NODE_ENV !== 'production') {
	const morgan = require("morgan");
	app.use(morgan("dev"));
}
app.use(express.json());

// Rutas
app.use("/api", require("./routes/backend.routes"));

// Iniciar el servidor
app.listen(app.get("port"), () => {
	console.log("Listening on port", app.get("port"));
});