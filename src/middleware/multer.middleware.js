/* Express */
const multer = require('multer');
/* Config */
const storageOpts = require("../config/multer.config")


var storage = multer.diskStorage(storageOpts)


module.exports = multer({ storage: storage }).single("carta");