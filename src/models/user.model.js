/* Config */
const pool = require("../config/database.config");
/* Models */
const BaseModel = require("./base.model");
/* Utils */
SqlQuery = require("../utils/sqlQuery.util");


class UserModel extends BaseModel {

	constructor() {
		super("users");
	}
}


module.exports = new UserModel();