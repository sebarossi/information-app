/* Config */
const pool = require("../config/database.config");
/* Models */
const BaseModel = require("./base.model");
/* Utils */
SqlQuery = require("../utils/sqlQuery.util");


class InformationModel extends BaseModel {

	constructor() {
		super("information", "device_id");
	}
}


module.exports = new InformationModel();