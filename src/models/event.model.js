/* Config */
const pool = require("../config/database.config");
/* Models */
const BaseModel = require("./base.model");
/* Utils */
SqlQuery = require("../utils/sqlQuery.util");


class EventModel extends BaseModel {

	constructor() {
		super("events");
	}

	getAffectedUsers(filters, fieldName, whereObj = [], operationFilters = []) {
		var queryObj = new SqlQuery().selectFrom(this.tableName)
			.operationField("distinct", "information.fcm_token", fieldName)
			.filters(filters);
		if (whereObj != null) {
		}
		whereObj.forEach(whereData => {
			queryObj.where(whereData.baseField, whereData.condition, whereData.secondField, whereData.andOr, whereData.literalField);
		});
		operationFilters.forEach(operationObj => {
			queryObj.operationWhere(operationObj.baseField, operationObj.condition, operationObj.values)
		});
		queryObj = queryObj
			.innerJoin("information", "device_id", "information_id")
			.prepareQuery();
		return new Promise((resolve, reject) => {
			pool.query(queryObj.queryString, queryObj.queryData, (err, results) => {
				if (err)
					return reject(err);
				if (results) {
					return resolve(results);
				}
			});
		});
	};

	getLastFromInfo(infoId) {
		var queryObj = new SqlQuery().selectFrom(this.tableName)
			.filters([{
				information_id: infoId
			}])
			.limit(0, 1)
			.prepareQuery();
		console.log(queryObj);
		console.log(queryObj.queryString);
		console.log(queryObj.queryData);
		return new Promise((resolve, reject) => {
			pool.query(queryObj.queryString, queryObj.queryData, (err, results) => {
				if (err)
					return reject(err);
				if (results) {
					return resolve(results[0]);
				}
			});
		});
	};
}


module.exports = new EventModel();