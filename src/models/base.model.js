/* Config */
const pool = require("../config/database.config");
/* Utils */
SqlQuery = require("../utils/sqlQuery.util");


class BaseModel {

	constructor(tableName, pkField = "id") {
		this.tableName = tableName;
		this.pkField = pkField;
	}

	getAll(populateOption = false) {
		var nestTables = false;
		var queryObj = new SqlQuery().selectFrom(this.tableName);

		if (populateOption) {
			this.populateTables.forEach(populate => {
				queryObj.leftJoin(populate.joinedTable, populate.joinedField, populate.baseField)
					.groupByField(this.tableName + "." + this.pkField);
				nestTables = true;
			});
		}

		queryObj = queryObj.prepareQuery();
		var options = { sql: queryObj.queryString, nestTables: nestTables };

		return new Promise((resolve, reject) => {
			pool.query(options, queryObj.queryData, (err, results) => {
				if (err) {
					return reject(err);
				}
				if (results)
					return resolve(results);
			});
		});
	};

	getOne(id, populateOption = false) {
		var filterObj = {};
		filterObj[this.tableName + "." + this.pkField] = id;

		var queryObj = new SqlQuery().selectFrom(this.tableName).filter(filterObj);
		var nestTables = false;

		if (populateOption) {
			this.populateTables.forEach(populate => {
				queryObj.leftJoin(populate.joinedTable, populate.joinedField, populate.baseField);
				nestTables = true;
			});
		}

		queryObj.groupByField(this.tableName + "." + this.pkField);
		queryObj = queryObj.prepareQuery();
		var options = { sql: queryObj.queryString, nestTables: nestTables };

		return new Promise((resolve, reject) => {
			pool.query(options, queryObj.queryData, (err, results) => {
				if (err) {
					return reject(err);
				}
				if (results)
					return resolve(results[0]);
			});
		});
	};

	new(newObj, returnAttr = "insertId") {
		var queryStringColumns = "";
		var queryStringValues = "";
		var queryDataColumns = [];
		var queryDataValues = [];

		Object.keys(newObj).forEach(column => {
			queryStringColumns += "??, ";
			queryDataColumns.push(column);
			queryStringValues += "?, ";
			queryDataValues.push(newObj[column]);
		});

		queryStringColumns = queryStringColumns.slice(0, -2);
		queryStringValues = queryStringValues.slice(0, -2);
		var queryString = "INSERT INTO " + this.tableName + " (" + queryStringColumns + ") VALUES (" + queryStringValues + ")";
		var queryData = queryDataColumns.concat(queryDataValues);
		return new Promise((resolve, reject) => {
			pool.query(queryString, queryData, (err, results) => {
				if (err) {
					return reject(err);
				}
				if (results)
					return resolve(results[returnAttr]);
			});
		});
	}

	update(id, data) {
		return new Promise((resolve, reject) => {
			pool.query("UPDATE ?? SET ? WHERE ?? = ?", [this.tableName, data, this.pkField, id], (err, results) => {
				if (err) {
					return reject(err);
				}
				if (results)
					return resolve(results);
			});
		});
	}

	delete(id, idField = "id") {
		return new Promise((resolve, reject) => {
			pool.query("DELETE FROM ?? WHERE ?? = ?", [this.tableName, idField, id], (err, results) => {
				if (err) {
					return reject(err);
				}
				if (results)
					return resolve(results.affectedRows);
			});
		});
	}

	exists(filters, countField = "id") {
		var queryObj = new SqlQuery().selectFrom(this.tableName)
			.operationField("count", countField, "count")
			.filters(filters)
			.prepareQuery();
		return new Promise((resolve, reject) => {
			pool.query(queryObj.queryString, queryObj.queryData, (err, results) => {
				if (err)
					return reject(err);
				if (results) {
					return resolve(results[0].count > 0);
				}
			});
		});
	}

	getOneByField(fieldOption) {
		var queryObj = new SqlQuery().selectFrom(this.tableName)
			.filter(fieldOption)
			.prepareQuery();
		return new Promise((resolve, reject) => {
			pool.query(queryObj.queryString, queryObj.queryData, (err, results) => {
				if (err) {
					return reject(err);
				}
				if (results)
					return resolve(results[0]);
			});
		});
	};

	getAllExcept(field, exceptValues) {
		var queryObj = new SqlQuery().selectFrom(this.tableName)
			.operationWhere(field, "NOT IN", exceptValues)
			.prepareQuery();
		return new Promise((resolve, reject) => {
			pool.query(queryObj.queryString, queryObj.queryData, (err, results) => {
				if (err) {
					return reject(err);
				}
				if (results)
					return resolve(results);
			});
		});
	};

	count(filters, whereObj = [], countField = "id") {
		var queryObj = new SqlQuery().selectFrom(this.tableName)
			.operationField("count", countField, "count")
			.filters(filters);
		if (whereObj != null) {
		}
		whereObj.forEach(whereData => {
			queryObj.where(whereData.baseField, whereData.condition, whereData.secondField, whereData.andOr, whereData.literalField);
		});
		queryObj = queryObj.prepareQuery();
		return new Promise((resolve, reject) => {
			pool.query(queryObj.queryString, queryObj.queryData, (err, results) => {
				if (err)
					return reject(err);
				if (results) {
					return resolve(results[0].count);
				}
			});
		});
	};

	filter(filters, whereObj = null, fields = ["*"]) {
		var queryObj = new SqlQuery().selectFrom(this.tableName)
			.fields(fields)
			.filters(filters);
		if (whereObj != null) {
			queryObj.where(whereObj.baseField, whereObj.condition, whereObj.secondField, whereObj.andOr, whereObj.literalField);
		}
		queryObj = queryObj.prepareQuery();
		return new Promise((resolve, reject) => {
			pool.query(queryObj.queryString, queryObj.queryData, (err, results) => {
				if (err)
					return reject(err);
				if (results) {
					return resolve(results);
				}
			});
		});
	};
}


module.exports = BaseModel;
