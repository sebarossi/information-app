/* Models */
const informationModel = require("../models/information.model");
/* Controllers */
const BaseController = require("./base.controller");
/* Utils */
const validator = require("../utils/valiadtor.util");


class InformationController extends BaseController {

	constructor() {
		super(informationModel, "information", "informations");
		this.requiredFields = [
			"device_id",
			"name",
			"lastName",
			"document",
			"address"
		];
	}

	create(req, res, next) {
		var validationObj = [
			{
				function: validator.requierdFields,
				data: {
					fieldList: [...this.requiredFields, "fcm_token"],
					values: req.body
				}
			},
			{
				function: validator.devId,
				data: req.body.device_id
			},
			{
				function: validator.name,
				data: req.body.name
			},
			{
				function: validator.name,
				data: req.body.lastName
			},
			{
				function: validator.address,
				data: req.body.address
			}
		];
		console.log(req.body);

		validator.validate(validationObj)
			.then(() => {
				// Verificar si ya existe la información
				return this.model.exists([{ device_id: req.body.device_id }], "device_id");
			})
			.then((existingInformation) => {
				if (existingInformation) {
					throw "ya existe la información de este dispositivo";
				}
				// Crear la información en la DB
				var newObj = {
					device_id: req.body.device_id,
					name: req.body.name,
					last_name: req.body.lastName,
					document: req.body.document,
					address: req.body.address,
					fcm_token: req.body.fcm_token,
				}
				return this.model.new(newObj, "affectedRows");
			})
			.then((affectedRows) => {
				if (affectedRows <= 0) {  // La información no se creó
					throw "error creando la entidad";
				}
				console.log("entidad creada");
				return this.model.getOne(req.body.device_id);
			})
			.then((newInformation) => {
				quickResponse(req, res, next, status.OK, true, "", { information: newInformation });
			})
			.catch((error) => { // Error creando la información
				console.log("error interno creando la información", error);
				quickResponse(req, res, next, status.internalServerError, false, "error interno creando la entidad", null, error);
				return;
			});
	}

	update(req, res, next) {
		var validationObj = [
			{
				function: validator.requierdFields,
				data: {
					fieldList: this.requiredFields,
					values: req.body
				}
			},
			{
				function: validator.devId,
				data: req.body.device_id
			},
			{
				function: validator.name,
				data: req.body.name
			},
			{
				function: validator.name,
				data: req.body.lastName
			},
			{
				function: validator.address,
				data: req.body.address
			}
		];

		validator.validate(validationObj)
			.then(() => {
				// Verificar si ya existe la información
				return this.model.exists([{ device_id: req.body.device_id }], "device_id");
			})
			.then((existingInformation) => {
				if (!existingInformation) {
					throw "no existe la información de este dispositivo";
				}
				// Crear la información en la DB
				var updatedObj = {
					name: req.body.name,
					last_name: req.body.lastName,
					document: req.body.document,
					address: req.body.address,
				}
				return this.model.update(req.body.device_id, updatedObj);
			})
			.then((updateResult) => {
				// if (updateResult.changedRows <= 0) {  // La entidad no se acctualizó
				// 	throw "la entidad no necesita actualizarse";
				// }
				console.log("entidad actuailizada");
				return this.model.getOne(req.body.device_id);
			})
			.then((updatedInformation) => {
				quickResponse(req, res, next, status.OK, true, "", { information: updatedInformation });
			})
			.catch((error) => { // Error creando la información
				console.log("error interno creando la información", error);
				quickResponse(req, res, next, status.internalServerError, false, "error interno creando la entidad", null, error);
				return;
			});
	}

	delete(req, res, next) {
		var validationObj = [
			{
				function: validator.requierdFields,
				data: {
					fieldList: ["device_id"],
					values: req.body
				}
			},
			{
				function: validator.devId,
				data: req.body.device_id
			},
		];
		console.log(req.body);
		validator.validate(validationObj)
			.then(() => {
				// Verificar si ya existe la información
				return this.model.exists([{ device_id: req.body.device_id }], "device_id");
			})
			.then((existingInformation) => {
				if (!existingInformation) {
					throw "no existe la información de este dispositivo";
				}
				// Crear la información en la DB
				return this.model.delete(req.body.device_id, "device_id");
			})
			.then((affectedRows) => {
				if (affectedRows <= 0) {  // La entidad no se acctualizó
					throw "la entidad no necesita actualizarse";
				}
				quickResponse(req, res, next, status.OK, true, "", {});
			})
			.catch((error) => { // Error creando la información
				console.log("error interno borrando la información", error);
				quickResponse(req, res, next, status.internalServerError, false, "error interno borrando la entidad", null, error);
				return;
			});
	}
}


module.exports = new InformationController();
