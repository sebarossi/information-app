/* Express */
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const path = require('path');
const fs = require('fs');
/* Cofig */
const passport = require("passport");
/* Models */
const userModel = require("../models/user.model");
/* Controllers */
const BaseController = require("./base.controller");
/* Utils */
const { isAuthenticated } = require("../utils/authentication.util");


class UserController extends BaseController {

	constructor() {
		super(userModel, "user", "users");
		this.requiredFields = [
			"email",
			"password",
			"repeatPassword",
			"name",
			"address"
		];
	}

	create(req, res, next) {
		console.log("creando entidad USER");

		var validationObj = [
			{
				function: validator.requierdFields,
				data: {
					fieldList: this.requiredFields,
					values: req.body
				}
			},
			{
				function: validator.email,
				data: req.body.email
			},
			{
				function: validator.name,
				data: req.body.name
			},
			{
				function: validator.address,
				data: req.body.address
			}
		];

		validator.validate(validationObj)
			.then(() => {
				// Verificar si ya existe el usuario
				return this.model.exists([{ email: req.body.email }]);
			})
			.then((existingUser) => {
				if (existingUser) {
					throw "ya existe un usuario con la dirección ingresada";
				}

				// Verificar si coinciden las contraseñas del formulario
				if (req.body.password != req.body.repeatPassword) {
					throw "no coinciden las contraseñas del formulario";
				}
				return bcrypt.hash(req.body.password, 10);
			})
			.then((securePassword) => {
				// Crear el usuario en la DB
				var newObj = {
					email: req.body.email,
					password: securePassword,
					name: req.body.name,
					address: req.body.address
				}
				return this.model.new(newObj)
			})
			.then((insertedId) => {
				if (!insertedId) {  // El usuario no se creó
					throw "error creando la entidad";
				}
				console.log("entidad creada");
				return this.model.getOne(insertedId)
			})
			.then((newUser) => {
				quickResponse(req, res, next, status.OK, true, "", { user: newUser });
			})
			.catch((error) => { // Error creando el usuario
				console.log("error interno creando el usuario");
				quickResponse(req, res, next, status.internalServerError, false, "error interno creando el usuario", null, error);
				return;
			});
	}

	update(req, res, next) {
		var validationObj = [
			{
				function: validator.requierdFields,
				data: {
					fieldList: [
						"id",
						"email",
						"name",
						"address"
					],
					values: req.body
				}
			},
			{
				function: validator.email,
				data: req.body.email
			},
			{
				function: validator.name,
				data: req.body.name
			},
			{
				function: validator.address,
				data: req.body.address
			}
		];

		validator.validate(validationObj)
			.then(() => {
				// Verificar si ya existe el usuario
				return this.model.exists([{ id: req.body.id }], "id");
			})
			.then((existingUser) => {
				if (!existingUser) {
					throw "no existe este usuario";
				}
				// Crear el usuario en la DB
				var updatedObj = {
					name: req.body.name,
					address: req.body.address,
					email: req.body.email,
					address: req.body.address,
				}
				console.log(updatedObj);
				return this.model.update(req.body.id, updatedObj);
			})
			.then((updateResult) => {
				// if (updateResult.changedRows <= 0) {  // La entidad no se actualizó
				// 	throw "la entidad no necesita actualizarse";
				// }
				console.log("entidad actuailizada");
				return this.model.getOne(req.body.id)
			})
			.then((updatedUser) => {
				quickResponse(req, res, next, status.OK, true, "", { user: updatedUser });
			})
			.catch((error) => { // Error creando el usuario
				console.log("error interno creando el usuario", error);
				quickResponse(req, res, next, status.internalServerError, false, "error interno creando la entidad", null, error);
				return;
			});
	}

	delete(req, res, next) {
		console.log("eliminando entidad USER con id:", req.params.id);
		res.status(200).json({
			success: true,
			data: {},
			message: "Todo OK"
		});
	}

	info(req, res, next) {
		isAuthenticated(req, res, next, (error, user, info) => {
			if (!error) {
				var data = {
					login: user != null,
					user: user
				}
				return quickResponse(req, res, next, 200, true, info, data);
			}
			else
				return quickResponse(req, res, next, 200, false, error);
		})
	}

	login(req, res, next) {
		console.log("login de usuario:", req.body.email);
		if (req.isAuthenticated()) {
			quickResponse(req, res, next, status.OK, true, "El usuario ya inició sesión", { user: req.user });
			return;
		}
		// Verificar que estén todos los campos compeltos
		if (!(req.body.email && req.body.password)) {
			console.log("faltan campos en el formulario");
			quickResponse(req, res, next, status.OK, false, "Debe llenar todos los campos del formulario");
			return;
		}
		passport.authenticate('local', { session: false }, (error, user, info) => {
			if (info) {
				console.log("advertencia en login: ", info);
				quickResponse(req, res, next, status.OK, false, info);
				return;
			}
			if (error) {
				console.log("error on login", error);
				quickResponse(req, res, next, status.internalServerError, false, error);
				return;
			}
			if (!user) {
				quickResponse(req, res, next, status.OK, false, "Usuario y contraseña incorrectos");
				return;
			}
			var payload = {
				sub: user.id,
				exp: Date.now() + parseInt(process.env.JWT_LIFETIME),
				email: user.email
			}
			const jwtToken = jwt.sign(JSON.stringify(payload), process.env.JWT_SECRET, { algorithm: process.env.JWT_ALGORITHM })
			// res.cookie('Authorization', jwtToken);
			quickResponse(req, res, next, status.OK, true, "Usuario autenticado", { user: user, token: jwtToken })
			return;
		})(req, res, next);
	}

	upload(req, res, next) {
		var filePath = process.env.UPLOAD_DIR + "/" + req.body.id + ".pdf";
		// Actualiza el campo carta del usuario
		var updatedObj = {
			carta: filePath,
		}
		return this.model.update(req.body.id, updatedObj)
			.then((updateResult) => {
				console.log("entidad actuailizada");
				quickResponse(req, res, next, status.OK, true, "Archivo guardado", { filePath: filePath });
				return;
			})
			.catch((error) => { // Error actualizando el usuario
				console.log("error interno actualizando el usuario", error);
				quickResponse(req, res, next, status.internalServerError, false, "error interno actualizando la entidad", null, error);
				return;
			});
	}

	uploadError(err, req, res, next) {
		if (err) {
			console.log("multer error", err);
			quickResponse(req, res, next, status.OK, false, "Error guardando archivo", { filePath: null }, err)
		}
		else {
			next();
		}
	}

	file(req, res, next) {
		var validationObj = [
			{
				function: validator.requierdFields,
				data: {
					fieldList: [
						"filePath",
					],
					values: req.body
				}
			},
			{
				function: validator.path,
				data: req.body.filePath
			},
		];

		validator.validate(validationObj)
			.then(() => {
				var filePath = req.body.filePath;
				var absolutePath = path.resolve(filePath);
				if (!fs.existsSync(absolutePath)) {
					throw "No existe el archivo";
				}
				res.sendFile(absolutePath);
				return
			})
			.catch((error) => { // Error actualizando el usuario
				console.log("error accediendo al archivo", error);
				quickResponse(req, res, next, status.internalServerError, false, "No se pudo acceder al archivo", null, error);
				return;
			})
	}
}


module.exports = new UserController();