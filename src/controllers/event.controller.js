/* Config */
const admin = require("../config/firebase.config");
/* Models */
const eventModel = require("../models/event.model");
const informationModel = require("../models/information.model");
const userModel = require("../models/user.model");
const { lastIndexOf } = require("../utils/serverStatus.util");
/* Controllers */
const BaseController = require("./base.controller");


class EventController extends BaseController {

	constructor() {
		super(eventModel, "event", "events");
		this.requiredFields = [
			"device_id",
			"establishment_id",
			"qr_date"	// La fecha en la que se generó el QR
		];
		this.informationModel = informationModel;
		this.userModel = userModel;
	}

	create(req, res, next) {
		console.log("creando entidad EVENT");
		var validationObj = [
			{
				function: validator.requierdFields,
				data: {
					fieldList: this.requiredFields,
					values: req.body
				}
			},
			{
				function: validator.devId,
				data: req.body.device_id
			},
			{
				function: validator.devId,
				data: req.body.establishment_id
			},
			// {
			// 	function: validator.date,
			// 	data: req.body.qr_date
			// }
		];

		validator.validate(validationObj)
			.then(() => {
				// Verificar si ya existe la información
				return this.informationModel.exists([{ device_id: req.body.device_id }], "device_id");
			})
			.then((existingInformation) => {
				if (!existingInformation) {
					throw "no existe la información de este dispositivo";
				}
				return this.userModel.exists([{ id: req.body.establishment_id }]);
			})
			.then((existingUser) => {
				if (!existingUser) {
					throw "no existe el establecimiento";
				}
				// VALIDAR FECHA ACTUAL CONTRA CREACIÓN DEL QR
				// Crear el evento en la DB
				var newObj = {
					information_id: req.body.device_id,
					user_id: req.body.establishment_id,
					date: new Date()
				}
				return this.model.new(newObj, "insertId");
			})
			.then((insertId) => {
				console.log(insertId);
				if (!insertId) {  // el evento no se creó
					throw "error creando la entidad";
				}
				console.log("entidad creada");
				return this.model.getOne(insertId)
			})
			.then((newEvent) => {
				console.log(newEvent);
				quickResponse(req, res, next, status.OK, true, "", { event: newEvent });
			})
			.catch((error) => { // Error creando el evento
				console.log("error interno creando el evento", error);
				quickResponse(req, res, next, status.internalServerError, false, "error interno creando la entidad", null, error);
				return;
			});
	}

	getAffectedCount(req, res, next) {
		var validationObj = [
			{
				function: validator.requierdFields,
				data: {
					fieldList: [
						"alertDate",
						"userId"
					],
					values: req.body
				}
			},
		];

		validator.validate(validationObj)
			.then(() => {
				var alertDate = req.body.alertDate;
				var safePeriod = (24 * 60 * 60 * 1000) * 1; // 1 day
				var safeDate = new Date(alertDate);
				safeDate.setTime(safeDate.getTime() + safePeriod);
				var userId = req.body.userId;
				var whereObj = [{
					baseField: "events.date",
					condition: ">",
					secondField: alertDate,
				},
				{
					baseField: "events.date",
					condition: "<",
					secondField: safeDate,
				}];
				var filterObj = [{
					user_id: userId
				}];
				return this.model.count(filterObj, whereObj);
			})
			.then((affectedClients) => {
				console.log("Recuento de clientes realizado");
				quickResponse(req, res, next, status.OK, true, "", { affectedClients: affectedClients });
				return;
			})
			.catch((error) => {
				console.log("Error contando clientes afectados:", error);
				quickResponse(req, res, next, status.internalServerError, false, "Error contando clientes afectados", null, error);
				return;
			});
	}

	sendAlert(req, res, next) {

		var validationObj = [
			{
				function: validator.requierdFields,
				data: {
					fieldList: [
						"alertDate",
						"userId"
					],
					values: req.body
				}
			},
		];
		validator.validate(validationObj)
			.then(() => {
				var alertDate = new Date(req.body.alertDate);
				var safePeriod = (24 * 60 * 60 * 1000) * 1; // 1 day
				var safeDate = new Date(alertDate);
				safeDate.setTime(safeDate.getTime() + safePeriod);
				var formattedAlertDate = alertDate.getFullYear() + "-" + (alertDate.getMonth() + 1) + "-" + alertDate.getDate() + " " + alertDate.getHours() + ":" + alertDate.getMinutes() + ":" + alertDate.getSeconds()
				var formattedSafeDate = safeDate.getFullYear() + "-" + (safeDate.getMonth() + 1) + "-" + safeDate.getDate() + " " + safeDate.getHours() + ":" + safeDate.getMinutes() + ":" + safeDate.getSeconds()
				console.log(formattedAlertDate);
				console.log(formattedSafeDate);
				var userId = req.body.userId;
				var whereObj = [{
					baseField: "events.date",
					condition: ">",
					secondField: formattedAlertDate,
				},
				{
					baseField: "events.date",
					condition: "<",
					secondField: formattedSafeDate,
				}];
				var operationFilter = [{
					baseField: "information.fcm_token",
					condition: "IS NOT",
					values: "NULL"
				}]
				var filterObj = [{
					user_id: userId
				}];
				return this.model.getAffectedUsers(filterObj, "token", whereObj, operationFilter);
			})
			.then((tokenList) => {
				if (!tokenList[0]) {
					throw "No existen dispositivos para recibir la notificación";
				}
				tokenList = tokenList.map((row) => row["token"]);
				var defaultAlertMessage = "Esto significa que probablemente hayas estado en contacto con alguién que luego demostró sínomas de COVID";
				var defaultAlertTitle = "Un establecimiento envió una alerta";
				var alertMessage = req.body.alertMessage ? req.body.alertMessage : defaultAlertMessage;
				var alertTitle = req.body.alertTitle ? req.body.alertTitle : defaultAlertTitle;
				const message = {
					notification: {
						title: alertTitle,
						body: alertMessage
					},
					tokens: tokenList,
				}

				return admin.messaging().sendMulticast(message);
			})
			.then((response) => {
				console.log("Push enviada:", response);
				quickResponse(req, res, next, status.OK, true, "Notificación enviada", response)
			})
			.catch((error) => {
				console.log("Error mandando push:", error);
				quickResponse(req, res, next, status.internalServerError, false, "Error mandando push", null, error);
				return;
			});
	}

	lastFromInfo(req, res, next) {
		var validationObj = [
			{
				function: validator.requierdFields,
				data: {
					fieldList: [
						"devId",
					],
					values: req.params
				}
			},
		];

		validator.validate(validationObj)
			.then(() => {
				return this.model.getLastFromInfo(req.params.devId);
			})
			.then((lastEventData) => {
				quickResponse(req, res, next, status.OK, true, "", lastEventData);
				return;
			})
			.catch((error) => {
				console.log("Error obteniendo último evento del cliente:", error);
				quickResponse(req, res, next, status.internalServerError, false, "Error obteniendo último evento del cliente", null, error);
				return;
			});
	}
}


module.exports = new EventController();