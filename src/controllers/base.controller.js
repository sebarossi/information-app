/* Utils */
const quickResponse = require("../utils/quickResponse.util");
const status = require("../utils/serverStatus.util");


class BaseController {

	constructor(model, singularName, pluralName) {
		this.model = model;
		this.singularName = singularName;
		this.pluralName = pluralName;
	}

	getOne(req, res, next) {
		console.log("buscando entidad " + this.singularName + " con id:", req.params.id)
		var populate = req.params.option === "populate" ? true : false;
		this.model.getOne(req.params.id, populate)
			.then((resultObject) => {
				var returnedObject = {};
				returnedObject[this.singularName] = resultObject;
				if (resultObject) {
					quickResponse(req, res, next, status.OK, true, "", returnedObject);
				}
				else
					quickResponse(req, res, next, status.OK, true, "No existe la entidad " + this.singularName, returnedObject);
				return;
			})
			.catch((error) => {
				console.log("error obteniendo la entidad " + this.singularName, error);
				quickResponse(req, res, next, status.internalServerError, false, "error obteniendo la entidad", null, error);
				return;
			});
	}

	getAll(req, res, next) {
		console.log("obteniendo la lista de: " + this.pluralName);
		var populate = req.params.option === "populate" ? true : false;
		this.model.getAll(populate)
			.then((resultObjects) => {
				var returnedObject = {};
				returnedObject[this.pluralName] = resultObjects;
				if (resultObjects && resultObjects.length > 0) {
					quickResponse(req, res, next, status.OK, true, "", returnedObject);
				}
				else
					quickResponse(req, res, next, status.OK, true, "No existen entidades " + this.pluralName + " en DB", returnedObject);
				return;
			})
			.catch((error) => {
				console.log("error obteniendo las entidades " + this.pluralName, error);
				quickResponse(req, res, next, status.internalServerError, false, "error obteniendo las entidades " + this.pluralName, null, error);
				return;
			});
	}
}


module.exports = BaseController;