/* Express */
const express = require("express");
const router = express.Router();
/* Middleware */
var multerSingleFile = require("../../middleware/multer.middleware");
/* Controllers */
const userController = require("../../controllers/user.controller");


// Related endpoints
router.post("/login", userController.login.bind(userController));
router.get("/info", userController.info.bind(userController));
router.post('/upload', multerSingleFile, userController.uploadError.bind(userController), userController.upload.bind(userController));
router.post('/file', userController.file.bind(userController));
// ABM endpoints
router.get("/:id", userController.getOne.bind(userController));
router.get("/", userController.getAll.bind(userController));
router.post("/", userController.create.bind(userController));
router.put("/", userController.update.bind(userController));
router.delete("/", userController.delete.bind(userController));


module.exports = router;