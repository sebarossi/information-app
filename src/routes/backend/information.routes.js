/* Express */
const express = require("express");
const router = express.Router();
/* Controllers */
const informationController = require("../../controllers/information.controller");


// ABM endpoints
router.get("/:id/:option?", informationController.getOne.bind(informationController));
router.get("/", informationController.getAll.bind(informationController));
router.post("/", informationController.create.bind(informationController));
router.put("/", informationController.update.bind(informationController));
router.delete("/", informationController.delete.bind(informationController));


module.exports = router;
