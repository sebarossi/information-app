/* Express */
const express = require("express");
const router = express.Router();
/* Controllers */
const eventController = require("../../controllers/event.controller");


// Realted endpoints
router.post('/notification', eventController.sendAlert.bind(eventController));
router.post('/affectedCount', eventController.getAffectedCount.bind(eventController));
router.get('/lastFromInfo/:devId', eventController.lastFromInfo.bind(eventController));
// ABM endpoints
router.get("/:id", () => {console.log("one"); }, eventController.getOne.bind(eventController));
router.get("/", () => {console.log("all"); }, eventController.getAll.bind(eventController));
router.post("/", () => {console.log("post"); }, eventController.create.bind(eventController));


module.exports = router;