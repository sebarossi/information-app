/* Express */
const express = require("express");
const router = express.Router();


router.use("/event", require("./backend/event.routes"));
router.use("/information", require("./backend/information.routes"));
router.use("/user", require("./backend/user.routes"));

module.exports = router;