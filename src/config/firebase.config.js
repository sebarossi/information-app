const admin = require('firebase-admin');


admin.initializeApp({
	credential: admin.credential.cert({
		type: process.env.FIREBASE_FIELD_TYPE,
		project_id: process.env.FIREBASE_FIELD_PROJECT_ID,
		private_key_id: process.env.FIREBASE_FIELD_PRIVATE_KEY_ID,
		privateKey: process.env.FIREBASE_FIELD_PRIVATE_KEY.replace(/\\n/g, '\n'),
		client_email: process.env.FIREBASE_FIELD_CLIENT_EMAIL,
		client_id: process.env.FIREBASE_FIELD_CLIENT_ID,
		auth_uri: process.env.FIREBASE_FIELD_AUTH_URI,
		token_uri: process.env.FIREBASE_FIELD_TOKEN_URI,
		auth_provider_x509_cert_url: process.env.FIREBASE_FIELD_AUTH_PROVIDER_X509_CERT_URL,
		client_x509_cert_url: process.env.FIREBASE_FIELD_CLIENT_X509_CERT_URL,
	})
});


module.exports = admin;