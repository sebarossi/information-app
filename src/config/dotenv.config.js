if (process.env.NODE_ENV !== 'production') {
	try {
		require("dotenv-safe").config();
	}
	catch (error) {
		if (error.name == "MissingEnvVarsError")
			throw ("faltan definir variables de entorno: " + error.missing)
		else
			throw ("error en la creación de variables de entorno: " + error)
	}
}