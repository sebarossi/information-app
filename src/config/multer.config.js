const opts = {
	destination: (req, file, cb) => {
		cb(null, process.env.UPLOAD_DIR);
	},
	filename: (req, file, cb) => {
		console.log(file);
		if (file.mimetype != "application/pdf") {
			cb(new Error("Sólo se permiten archivos de tipo PDF"));
		}
		if (!req.body.id) {
			cb(new Error("Faltan campos en el formulario"));
		}
		else {
			cb(null, req.body.id + ".pdf");
		}
	},
}


module.exports = opts;