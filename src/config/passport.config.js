/* Express */
const passport = require("passport");
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const localStrategy = require("passport-local").Strategy;
const bcrypt = require("bcrypt");
/* Models */
const userModel = require("../models/user.model");


myPassport = (app) => {
	app.use(passport.initialize())
	app.use(passport.session())
	passport.serializeUser((user, done) => {
		// El segundo argumento es lo que se guarda del usuario en la sesión
		done(null, { id: user.id, type: user.type });
	});

	passport.deserializeUser((obj, done) => {
		userModel.getOne(obj.id).then((user) => {
			// El segundo argumento es lo que se guarda del usuario en la request
			delete user.password;
			delete user.created;
			done(null, user);
		}).catch((error) => {
			done(error, false);
		})
	});

	var opts = {
		jwtFromRequest: ExtractJwt.fromExtractors([
			ExtractJwt.fromAuthHeaderAsBearerToken()
		]),
		secretOrKey: process.env.JWT_SECRET,
		algorithms: [process.env.JWT_ALGORITHM]
	}

	passport.use("jwt", new JwtStrategy(opts, (jwtPayload, done) => {
        if (!jwtPayload) {
            return done("empty token", null)
        }
        userModel.getOne(jwtPayload.sub)
            .then(user => {
                if (!user) {
                    return done(null, false);
                }
                else
                    return done(null, user);
            })
            .catch(err => done(err, null));
    }));

	passport.use(new localStrategy(
		{
			usernameField: "email",
			session: false
		},
		(username, password, done) => {
			userModel.getOneByField({ "email": username }).then((user) => {
				console.log("autenticando usuario: ", username);
				if (!user) {
					console.log("Usuario no autenticado: no existe el usuario");
					return done(null, false);
				}
				bcrypt.compare(password, user.password).then((result) => {
					if (!result) {
						console.log("Usuario no autenticado: no coinciden las contraseñas");
						return done(null, false);
					}
					console.log("Usuario autenticado");
					return done(null, user);
				}).catch((error) => {
					console.log("error on login", error);
					done(error);
				});
			}).catch((error) => {
				console.log("error on login", error);
				done(error);
			});
		}
	));
}


module.exports = myPassport;