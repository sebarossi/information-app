quickResponse = (req, res, next, status, success, message, payload, errorData = null) => {
	error = errorData && errorData.message ? errorData.message : errorData;
	response = {
		success: success,
		message: message ? message : "",
		data: payload ? payload : {},
		error: error
	}
	res.status(status).json(response);
}


module.exports = quickResponse;