status = {
	OK: 200,
	created: 201,
	noContent: 204,
	notModified: 304,
	badRequest: 400,
	unauthorized: 401,
	forbidden: 403,
	notFound: 404,
	requestTimeout: 408,
	tooManyRequest: 409,
	internalServerError: 500,
	notImplemented: 501,
	serviceUnavailable: 503,
}


module.exports = status;