validator = {};


validator.requierdFields = (obj) => {
    var fieldList = obj.fieldList;
    var values = Object.keys(obj.values);
    if (!(values && fieldList && fieldList != [] && values != [])) {
        return "Faltan campos en el formulario";
    }
    var presentValues = values.filter(x => fieldList.includes(x));
    var compare = presentValues.length == fieldList.length && presentValues.every((v) => fieldList.indexOf(v) >= 0);
    return compare ? true : "Faltan campos en el formulario";
}

validator.email = (email) => {
    if (!email) return "Formato de email incorrecto";
    var emailRegEx = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
    return emailRegEx.test(email) ? true : "Formato de email incorrecto";
}

validator.name = (name) => {
    if (!name) return "Formato de nombre/apellido incorrecto";
    var accentedCharacters = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ";
    var nameRegEx = new RegExp("^[a-zA-Z" + accentedCharacters + " ]{1,45}$");
    return nameRegEx.test(name) ? true : "Formato de nombre/apellido incorrecto";
}

validator.address = (address) => {
    if (!address) return "Formato de dirección incorrecto";
    var accentedCharacters = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ";
    var addressRegEx = new RegExp("^[a-zA-Z" + accentedCharacters + "0-9 ]{1,255}$");
    return addressRegEx.test(address) ? true : "Formato de dirección incorrecto";
}

validator.phone = (phone) => {
    if (!phone) return "Formato de número de teléfono incorrecto";
    var phoneRegEx = new RegExp("^[\+\-0-9 ]{1,20}$");
    return phoneRegEx.test(phone) ? true : "Formato de número de teléfono incorrecto";
}

validator.document = (document) => {
    if (!document) return "Formato de documento incorrecto";
    var documentRegEx = new RegExp("^[\.\-0-9a-zA-Z ]{1,20}$");
    return documentRegEx.test(document) ? true : "Formato de documento incorrecto";
}

validator.devId = (devId) => {
    if (!devId) return "Formato de ID de dispositivo incorrecto";
    var devIdRegEx = new RegExp("^[0-9a-zA-Z]{1,20}$");
    return devIdRegEx.test(devId) ? true : "Formato de ID de dispositivo incorrecto";
}

validator.path = (filePath) => {
    if (!filePath) return "Formato de path de archivo incorrecto";
    var filePathRegEx = new RegExp("^[0-9a-zA-Z/]{1,20}[\.][0-9a-zA-Z/]{1,3}$");
    if (!filePath.startsWith(process.env.UPLOAD_DIR) || filePath.includes("..")) {
        return "Formato de path de archivo incorrecto 12";
    }
    return filePathRegEx.test(filePath) ? true : "Formato de path de archivo incorrecto";
}

validator.validate = (obj) => {
    return new Promise((resolve, reject) => {
        obj.forEach(validationObj => {
            var result = validationObj.function(validationObj.data);
            if (result !== true) {
                return reject(result);
            }
        });
        return resolve(true);
    });
}


module.exports = validator;