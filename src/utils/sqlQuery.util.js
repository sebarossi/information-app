/* Utils */
const mysql = require("mysql");


class SqlQuery {

	constructor() {
		this.SELECT = {
			fields: [],
			aliasFields: [],
			literalFields: [],
			operationFields: []
		};
		this.FROM = {}
		this.JOIN = {
			inner: [],
			left: [],
			join: []
		};
		this.WHERE = {
			simple: [],
			filter: [],
			operation: []
		};
		this.GROUPBY = [];
		this.LIMIT = [];

		return this;
	}

	prepareQuery = () => {

		var selectObj = this.prepareSelect();
		var fromObj = this.prepareFrom();
		var joinObj = this.prepareJoin();
		var whereObj = this.prepareWhere();
		var groupByObj = this.prepareGroupBy()
		var limitObj = this.prepareLimit();

		var queryString = selectObj.queryString + " "
			+ fromObj.queryString + " "
			+ joinObj.queryString + " "
			+ whereObj.queryString
			+ groupByObj.queryString
			+ limitObj.queryString;

		var queryData = selectObj.queryData
			.concat(fromObj.queryData)
			.concat(joinObj.queryData)
			.concat(whereObj.queryData)
			.concat(groupByObj.queryData)
			.concat(limitObj.queryData);

		return { queryString, queryData }
	}

	prepareSelect = () => {
		// SELECT
		var queryString = "SELECT ";
		var queryData = [];
		if (this.SELECT.fields.length > 0 || this.SELECT.aliasFields.length > 0 || this.SELECT.literalFields.length > 0 || this.SELECT.operationFields.length > 0) {
			if (this.SELECT.fields.length > 0) {
				// Simple fields
				this.SELECT.fields.forEach(selections => {
					queryString += "??, ";
					queryData.push(selections);
				});
			}
			if (this.SELECT.aliasFields.length > 0) {
				// Alias Fields
				this.SELECT.aliasFields.forEach(selections => {
					queryString += selections + ", ";
				});
			}
			if (this.SELECT.literalFields.length > 0) {
				// Alias Fields
				this.SELECT.literalFields.forEach(fieldObj => {
					queryString += "? as ??, ";
					var literal = Object.keys(fieldObj);
					var asField = fieldObj[literal];
					queryData.push(literal, asField);
				});
			}
			if (this.SELECT.operationFields.length > 0) {
				// Operatin Fields
				this.SELECT.operationFields.forEach(selections => {
					queryString += selections + ", ";
				});
			}
			queryString = queryString.slice(0, -2);
		}
		else {
			queryString += "*"
		}

		return { queryString, queryData }
	}

	prepareFrom = () => {
		// FROM
		var queryString = "FROM ??";
		var queryData = this.FROM;

		return { queryString, queryData }
	}

	prepareJoin = () => {
		// INNER JOIN
		var queryString = "";
		var queryData = [];
		if (this.JOIN.join.length > 0 || this.JOIN.left.length > 0 || this.JOIN.inner.length > 0) {
			if (this.JOIN.join.length > 0) {
				this.JOIN.join.forEach(joinObj => {
					queryString += "JOIN ?? ON ?? " + joinObj.condition + " ??"
					queryData.push(joinObj.table, joinObj.joinedTableField, joinObj.baseTableField);
				});
			}
			if (this.JOIN.left.length > 0) {
				this.JOIN.left.forEach(joinObj => {
					queryString += "LEFT JOIN ?? ON ?? " + joinObj.condition + " ??"
					queryData.push(joinObj.table, joinObj.joinedTableField, joinObj.baseTableField);
				});
			}
			if (this.JOIN.inner.length > 0) {
				this.JOIN.inner.forEach(joinObj => {
					queryString += "INNER JOIN ?? ON ?? " + joinObj.condition + " ??";
					queryData.push(joinObj.table, joinObj.joinedTableField, joinObj.baseTableField);
				});
			}
		}

		return { queryString, queryData }
	}

	prepareWhere = () => {
		// WHERE
		var queryString = "";
		var queryData = [];
		if (this.WHERE.simple.length > 0 || this.WHERE.filter.length > 0 || this.WHERE.operation.length > 0) {
			var firstWhere = true;
			queryString += "WHERE";

			if (this.WHERE.filter.length > 0) {
				// FILTER
				this.WHERE.filter.forEach(filterObj => {
					if (!firstWhere) {
						queryString += " " + filterObj.andOr + " ";
					}
					else {
						queryString += " ( ";
					}
					firstWhere = false;
					queryString += "( ? ) "
					queryData.push(filterObj.filter);
				});
				queryString += " ) ";
			}
			if (this.WHERE.simple.length > 0) {
				// SIMPLE
				this.WHERE.simple.forEach(whereObj => {
					if (!firstWhere) {
						queryString += whereObj.andOr + " ";
					}
					firstWhere = false;
					if (typeof (whereObj.baseField) == "string" && whereObj.baseField.includes(".")) {
						queryString += " ( ?? " + whereObj.condition;
					}
					else {
						queryString += " ( ? " + whereObj.condition;
					}
					if (typeof (whereObj.secondField) == "string" && whereObj.secondField.includes(".")) {
						queryString += " ?? ) ";
					}
					else {
						queryString += " ? ) ";
					}
					queryData.push(whereObj.baseField, whereObj.secondField);
				});
			}
			if (this.WHERE.operation.length > 0) {
				// OPERATION
				this.WHERE.operation.forEach(whereObj => {
					if (!firstWhere) {
						queryString += whereObj.andOr + " ";
						firstWhere = false;
					}
					queryString += " ( ?? " + whereObj.condition;
					queryData.push(whereObj.field)
					switch (whereObj.condition) {
						case "IN": case "NOT IN":
							queryString += " ( ? ) ) ";
							queryData.push(whereObj.values);
							break;
						case "BETWEEN": case "NOT BETWEEN":
							queryString += " ? AND ? ) ";
							queryData.push(whereObj.values[0]);
							queryData.push(whereObj.values[1]);
							break;
						case "IS": case "IS NOT":
							if (whereObj.values != "NULL") {
								queryData.push(whereObj.values);
								queryString += " ? ) ";
							}
							else {
								queryString += " NULL ) ";
							}
							break;
						default:
							queryString += " ? ) ";
							queryData.push(whereObj.values);
							break;
					}
				});
			}
		}

		return { queryString, queryData }
	}

	prepareGroupBy = () => {
		var queryString = "";
		var queryData = [];
		if (this.GROUPBY.length > 0) {
			queryString += "GROUP BY ";
			this.GROUPBY.forEach(field => {
				queryString += "??, ";
				queryData.push(field);
			});
			queryString = queryString.slice(0, -2);
		}

		return { queryString, queryData }
	}

	prepareLimit = () => {
		var queryString = "";
		var queryData = [];
		if (Object.keys(this.LIMIT).length > 0) {
			var queryString = "LIMIT ?"
			var queryData = [this.LIMIT.from]
			if (this.LIMIT.count) {
				queryString += ", ?";
				queryData.push(this.LIMIT.count);
			}
		}
		return { queryString, queryData }
	}

	selectFrom = (from) => {
		this.FROM = from
		return this;
	}

	fields = (fieldList = ["*"]) => {
		fieldList.forEach(field => {
			this.field(field);
		});
		return this;
	}

	field = (field = "*") => {
		this.SELECT.fields.push(field);
		return this;
	}

	aliasFields = (aliasFieldList) => {
		aliasFieldList.forEach(field => {
			this.aliasField(field);
		});
		return this;
	}

	aliasField = (fieldObj) => {

		var value = Object.keys(fieldObj);
		var formattedValue = mysql.format("??", value);
		var asField = fieldObj[value];

		this.SELECT.aliasFields.push(formattedValue + " as '" + asField + "'");
		return this;
	}

	literalFields = (fieldList) => {
		fieldList.forEach(fieldObj => {
			this.literalField(fieldObj)
		});
		return this;
	}

	literalField = (fieldObj) => {
		this.SELECT.literalFields.push(fieldObj);
		return this;
	}

	operationFields = (operationFieldList) => {
		operationFieldList.forEach(field => {
			this.aliasField(field[0], field[1], field[2]);
		});
		return this;
	}

	operationField = (operation, field, asField) => {
		var sentence = mysql.format(operation + "(??) AS '" + asField + "'", field);
		this.SELECT.operationFields.push(sentence);
		return this;
	}

	where = (baseField, condition, secondField, andOr = "AND") => {
		this.WHERE.simple.push({
			baseField: baseField,
			condition: condition,
			secondField: secondField,
			andOr: andOr
		})
		return this;
	}

	filters = (filterList, andOr = "AND") => {
		filterList.forEach(filterObj => {
			this.filter(filterObj);
		});
		return this;
	}

	filter = (filterObj, andOr = "AND") => {
		this.WHERE.filter.push({
			filter: filterObj,
			andOr: andOr
		});
		return this;
	}

	operationWhere = (baseField, condition, values, andOr = "AND") => {
		this.WHERE.operation.push({
			field: baseField,
			condition: condition,
			values: values,
			andOr: andOr
		})
		return this;
	}

	leftJoin = (joinedTable, joinedField, baseTableField, condition = "=") => {
		var joinedTableField = joinedTable + "." + joinedField;
		this.JOIN.left.push({
			table: joinedTable,
			joinedTableField: joinedTableField,
			condition: condition,
			baseTableField: baseTableField
		});
		return this;
	}

	innerJoin = (joinedTable, joinedField, baseTableField, condition = "=") => {
		var joinedTableField = joinedTable + "." + joinedField;
		this.JOIN.inner.push({
			table: joinedTable,
			joinedTableField: joinedTableField,
			condition: condition,
			baseTableField: baseTableField
		});
		return this;
	}

	join = (joinedTable, joinedTableField, baseTableField, condition = "=") => {
		var joinedTableField = joinedTable + "." + joinedField;
		this.JOIN.join.push({
			table: joinedTable,
			joinedTableField: joinedTableField,
			condition: condition,
			baseTableField: baseTableField
		});
		return this;
	}

	groupByField = (fieldList) => {
		fieldList.forEach(field => {
			this.groupByField(field)
		});
		return this;
	}

	groupByField = (field) => {
		this.GROUPBY.push(field);
		return this;
	}

	limit = (from, count) => {
		this.LIMIT = {
			from: from,
			count: count
		}
		return this;
	}

}


module.exports = SqlQuery;