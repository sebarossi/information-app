/* Express */
const passport = require('passport');


authentication= {};

authentication.isAuthenticated = (req, res, next, callback) => {
    passport.authenticate("jwt", { session: false }, (error, user, info) => {
        if (info) {
            console.log("advertencia en login: ", info.message);
            return callback(null, null, info.message);
        }
        if (error) {
            console.log("error on login", error.message);
            return callback(error, null, null);
        }
        if (!user) {
            console.log("no existe el usuario de la request");
            return callback(null, null, "no existe el usuario en la request");
        }
        return callback(null, user, null);
    })(req, res, next);
}


module.exports = authentication;