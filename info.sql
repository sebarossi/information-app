-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: _info_app
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `information_id` varchar(45) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `observations` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,1,'1','2020-11-03 20:07:54',NULL),(2,1,'1','2020-11-03 20:10:32',NULL),(3,1,'1','2020-11-03 20:11:49',NULL),(4,1,'1','2020-11-03 20:12:10',NULL),(5,1,'1','2020-11-03 20:12:37',NULL),(6,1,'1','2020-11-03 20:16:34',NULL),(7,1,'1','2020-11-03 20:17:26',NULL),(8,1,'1','2020-11-03 20:20:11',NULL),(9,1,'1','2020-11-03 20:20:58',NULL),(10,1,'1','2020-11-03 20:21:38',NULL),(11,1,'1','2020-11-03 20:22:05',NULL),(12,1,'1','2020-11-03 20:26:37',NULL),(13,1,'1','2020-11-03 20:30:24',NULL),(14,1,'1','2020-11-03 20:31:37',NULL),(15,1,'1','2020-11-03 20:33:33',NULL),(16,1,'1','2020-11-03 20:34:03',NULL),(17,1,'1','2020-11-03 20:35:12',NULL),(18,1,'1','2020-11-03 20:35:39',NULL),(19,1,'1','2020-12-27 16:25:02',NULL),(20,1,'1','2020-12-27 17:26:01',NULL),(21,1,'1','2020-12-27 18:20:00',NULL),(22,1,'1','2020-12-27 18:20:17',NULL),(23,1,'1','2020-12-27 18:20:46',NULL),(24,1,'1fa97f5bbf28be8e','2020-12-27 18:52:58',NULL),(25,1,'1fa97f5bbf28be8e','2020-12-27 18:56:54',NULL),(26,1,'1fa97f5bbf28be8e','2020-12-27 18:57:30',NULL),(27,1,'1fa97f5bbf28be8e','2020-12-27 18:59:07',NULL),(28,1,'1fa97f5bbf28be8e','2020-12-27 18:59:43',NULL),(29,1,'1fa97f5bbf28be8e','2020-12-27 19:00:30',NULL);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `information`
--

DROP TABLE IF EXISTS `information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information` (
  `device_id` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `document` varchar(45) NOT NULL,
  `address` tinytext NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `cellphone_number` varchar(20) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `fcm_token` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`device_id`),
  UNIQUE KEY `device_id_UNIQUE` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `information`
--

LOCK TABLES `information` WRITE;
/*!40000 ALTER TABLE `information` DISABLE KEYS */;
INSERT INTO `information` VALUES ('','Sebastián','Rossi','39951271','San Martín 1466',NULL,NULL,NULL,NULL),('12','lalo','rossi','12','12',NULL,NULL,NULL,NULL),('123','sebastian','rossi','39951271','san martín 1466',NULL,NULL,NULL,NULL),('1a6fa6f0010eecc0','Sebastián','Rossi','39951271','San Martín 1466',NULL,NULL,NULL,'exduWmurSvWsyapVdQf5QZ:APA91bGZH7nxj-an5eS7oaebKrqLB6m1pQmcvTW02rD2lGj46v9wGfb275gLwDpkHfbQ-JKPYjXWxD7IOm2R0yHquWcZzq-KjnonMWbLafWPWjkwhQJ8faaU0EJ0PXutkUN9ix7EQY9x'),('1a6fa6f0010eecc9','Sebastián','Rossi','39951271','San Martín 1466',NULL,NULL,NULL,'exduWmurSvWsyapVdQf5QZ:APA91bGZH7nxj-an5eS7oaebKrqLB6m1pQmcvTW02rD2lGj46v9wGfb275gLwDpkHfbQ-JKPYjXWxD7IOm2R0yHquWcZzq-KjnonMWbLafWPWjkwhQJ8faaU0EJ0PXutkUN9ix7EQY9x'),('1a6fa6f9910eecc9','Sebastián','Rossi','39951271','San Martín 1466',NULL,NULL,NULL,'exduWmurSvWsyapVdQf5QZ:APA91bGZH7nxj-an5eS7oaebKrqLB6m1pQmcvTW02rD2lGj46v9wGfb275gLwDpkHfbQ-JKPYjXWxD7IOm2R0yHquWcZzq-KjnonMWbLafWPWjkwhQJ8faaU0EJ0PXutkUN9ix7EQY9x'),('1fa97f5bbf28be8e','test','test','39999999','testqwer',NULL,NULL,NULL,'exduWmurSvWsyapVdQf5QZ:APA91bGZH7nxj-an5eS7oaebKrqLB6m1pQmcvTW02rD2lGj46v9wGfb275gLwDpkHfbQ-JKPYjXWxD7IOm2R0yHquWcZzq-KjnonMWbLafWPWjkwhQJ8faaU0EJ0PXutkUN9ix7EQY9x'),('c8a7a3d82b4c6b42','test','test','39999999','test1322',NULL,NULL,NULL,NULL),('_c8a7a3d82b4c6b42','sebastian','rossi','39951271','San Marino 1466',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `address` tinytext,
  `name` varchar(45) DEFAULT NULL,
  `carta` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'test@test.com','$2b$10$u7tDGsi7yOZFJ.MW/Kgcd.ENBrHXaD/lruGmLEbx0DN7Y0mphLQpK','12','lalo','uploads/1.pdf'),(2,'bardelaesquina@test.com','$2b$10$5YmU7UqduYq13T0yrTT0IOR/i7i.xvX.A1v6dpgRboGvdWxVcBN8O','la esquina','bardelaesquina',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database '_info_app'
--

--
-- Dumping routines for database '_info_app'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-16 23:36:58
